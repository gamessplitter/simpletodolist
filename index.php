<?php
//random number to prevent caching
$rand =rand(1, 1000000);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Todo</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body onload="">
	<div id="myDIV" class="header">
	  <h2>My To Do List</h2>
	  <input type="text" id="myInput" placeholder="Title...">
	  <span onclick="newElement()" class="addBtn">Add</span>
	</div>
	<ul id="myUL">
	  <li>Loading</li>
	</ul>
	<script src="list.js?rand=<?php echo $rand ?>"></script>
	<script>
		loadier();
	</script>
</body>
</html>