//object length function from // https://stackoverflow.com/questions/16976904/javascript-counting-number-of-objects-in-object

function ObjectLength( object ) {
    var length = 0;
    for( var key in object ) {
        if( object.hasOwnProperty(key) ) {
            ++length;
        }
    }
    return length;
};

// Click on a close button to hide the current list item

function remove(list_item){
  sure = confirm('Are you sure you want to delete the element?');
  if(sure){
    var div = list_item.parentElement;
    div.parentNode.removeChild(div);
  }
  list_item.parentNode.removeChild(list_item);
  save();
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('checked');
    save();
  }
}, false);

// Create a new list item when clicking on the "Add" button
function newElement() {
  var li = document.createElement("li");
  var inputValue = document.getElementById("myInput").value;
  var t = document.createTextNode(inputValue);
  li.classList.add('column');
  li.setAttribute('draggable','true');
  li.appendChild(t);
  if (inputValue === '') {
    alert("You must write something!");
  } else {
    document.getElementById("myUL").appendChild(li);
  }
  document.getElementById("myInput").value = "";

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  span.addEventListener("click", remove, false);
  li.appendChild(span);
  save();
}

function loadier(){
    var client = new XMLHttpRequest();
    ramdonizer = Math.floor((Math.random() * 1000) + 1); //to prevent caching
    processed = false;
    if(processed==false){ //Don't know why it runs multiple times, make sure it only runs once
        client.open('GET', 'current_list.txt?rnd=' + ramdonizer);
        client.onreadystatechange = function() {
            if(client.responseText!='' && client.responseText != null){
                list_items = JSON.parse(client.responseText);
                content = buildInnerHtml(list_items);
                document.getElementById('myUL').innerHTML = content;
            }
        }
        processed = true;
        client.send();
    }
}
function buildInnerHtml(object){
    inner_string = '';
    for (var i = 0; i < ObjectLength(object); ++i) {
        if(object[i]['checked']==true){
            checked_class = ' checked'
        }else{
            checked_class = '';
        }
        inner_string += '<li class="column'+checked_class+'">' + object[i]['text'] + '<span class="close" onclick="remove(this)">x</span></li>';
    }
    return inner_string;
    
}

function save(){
    result_object = {}
    var ul = document.getElementById("myUL");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
        result_object[i] = {};
        result_object[i]['text'] = items[i].innerHTML.replace(/<span class="close" onclick="remove(this)">(.*?)<\/span>/g, '');
        result_object[i]['checked'] = items[i].classList.contains('checked'); 
    }
    json_list = JSON.stringify(result_object);
    
    var data = new FormData();
    data.append("data" , json_list);
    var xhr = (window.XMLHttpRequest) ? new XMLHttpRequest() : new activeXObject("Microsoft.XMLHTTP");
    xhr.open( 'post', 'save_list.php', true );
    xhr.send(data);
}

//code to allow drag and drop the list items.